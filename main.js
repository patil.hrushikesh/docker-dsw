(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/aboutus/aboutus.module.ts":
/*!*******************************************!*\
  !*** ./src/app/aboutus/aboutus.module.ts ***!
  \*******************************************/
/*! exports provided: AboutusModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutusModule", function() { return AboutusModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./aboutus/aboutus.component */ "./src/app/aboutus/aboutus/aboutus.component.ts");
/* harmony import */ var _sanitize_html_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sanitize-html.pipe */ "./src/app/sanitize-html.pipe.ts");




var AboutusModule = /** @class */ (function () {
    function AboutusModule() {
    }
    AboutusModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_2__["AboutusComponent"], _sanitize_html_pipe__WEBPACK_IMPORTED_MODULE_3__["SanitizeHtmlPipe"]],
            providers: [_sanitize_html_pipe__WEBPACK_IMPORTED_MODULE_3__["SanitizeHtmlPipe"]],
            exports: [_aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_2__["AboutusComponent"]]
        })
    ], AboutusModule);
    return AboutusModule;
}());



/***/ }),

/***/ "./src/app/aboutus/aboutus/aboutus.component.html":
/*!********************************************************!*\
  !*** ./src/app/aboutus/aboutus/aboutus.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"m-5\" [innerHTML]=\"aboutusText | sanitizeHtml\"></section>"

/***/ }),

/***/ "./src/app/aboutus/aboutus/aboutus.component.scss":
/*!********************************************************!*\
  !*** ./src/app/aboutus/aboutus/aboutus.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table, tr, th, td {\n  border: 1px solid black !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2hydXNoaWtlc2gvd29yay9kc3cvc3JjL2FwcC9hYm91dHVzL2Fib3V0dXMvYWJvdXR1cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtDQUFpQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvYWJvdXR1cy9hYm91dHVzL2Fib3V0dXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSwgdHIsdGgsIHRkIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBibGFjayFpbXBvcnRhbnQ7XG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/aboutus/aboutus/aboutus.component.ts":
/*!******************************************************!*\
  !*** ./src/app/aboutus/aboutus/aboutus.component.ts ***!
  \******************************************************/
/*! exports provided: AboutusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutusComponent", function() { return AboutusComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _collection_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../collection.service */ "./src/app/collection.service.ts");



var AboutusComponent = /** @class */ (function () {
    function AboutusComponent(collectionService) {
        this.collectionService = collectionService;
    }
    AboutusComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.collectionService.getAboutUs().subscribe(function (data) {
            console.log(data);
            _this.aboutusText = data.aboutusenglish;
        });
    };
    AboutusComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-aboutus',
            template: __webpack_require__(/*! ./aboutus.component.html */ "./src/app/aboutus/aboutus/aboutus.component.html"),
            styles: [__webpack_require__(/*! ./aboutus.component.scss */ "./src/app/aboutus/aboutus/aboutus.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_collection_service__WEBPACK_IMPORTED_MODULE_2__["CollectionService"]])
    ], AboutusComponent);
    return AboutusComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _homepage_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./homepage/home/home.component */ "./src/app/homepage/home/home.component.ts");
/* harmony import */ var _aboutus_aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./aboutus/aboutus/aboutus.component */ "./src/app/aboutus/aboutus/aboutus.component.ts");





var routes = [
    { path: 'aboutus', component: _aboutus_aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_4__["AboutusComponent"] },
    { path: '', component: _homepage_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<app-navbar></app-navbar>\n<router-outlet></router-outlet>\n<app-footer></app-footer>\n<app-copyrightfooter></app-copyrightfooter>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'dsw';
        this.images = [1, 2, 3].map(function () { return "https://picsum.photos/900/500?random&t=" + Math.random(); });
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _navbar_navbar_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./navbar/navbar.module */ "./src/app/navbar/navbar.module.ts");
/* harmony import */ var _homepage_homepage_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./homepage/homepage.module */ "./src/app/homepage/homepage.module.ts");
/* harmony import */ var _aboutus_aboutus_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./aboutus/aboutus.module */ "./src/app/aboutus/aboutus.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _copyrightfooter_copyrightfooter_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./copyrightfooter/copyrightfooter.component */ "./src/app/copyrightfooter/copyrightfooter.component.ts");












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"],
                _copyrightfooter_copyrightfooter_component__WEBPACK_IMPORTED_MODULE_11__["CopyrightfooterComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbModule"],
                _navbar_navbar_module__WEBPACK_IMPORTED_MODULE_5__["NavbarModule"],
                _homepage_homepage_module__WEBPACK_IMPORTED_MODULE_6__["HomepageModule"],
                _aboutus_aboutus_module__WEBPACK_IMPORTED_MODULE_7__["AboutusModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/collection.service.ts":
/*!***************************************!*\
  !*** ./src/app/collection.service.ts ***!
  \***************************************/
/*! exports provided: CollectionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CollectionService", function() { return CollectionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./language.service */ "./src/app/language.service.ts");





var CollectionService = /** @class */ (function () {
    function CollectionService(http, langService) {
        this.http = http;
        this.langService = langService;
    }
    CollectionService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    CollectionService.prototype.endPointCollections = function (collectionName) {
        return "/cockpit/api/collections/get/" + collectionName + "?token=48c21bd90fe7b4faef2c20cf33d761";
    };
    CollectionService.prototype.getPressLinks = function () {
        return this.http.get(this.endPointCollections('presslinks')).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.extractData));
    };
    CollectionService.prototype.endPointSingleton = function (singletonName) {
        return "/cockpit/api/singletons/get/" + singletonName + "?token=48c21bd90fe7b4faef2c20cf33d761";
    };
    CollectionService.prototype.getAboutUs = function () {
        return this.http.get(this.endPointSingleton('aboutus')).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.extractData));
    };
    CollectionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"]])
    ], CollectionService);
    return CollectionService;
}());



/***/ }),

/***/ "./src/app/copyrightfooter/copyrightfooter.component.html":
/*!****************************************************************!*\
  !*** ./src/app/copyrightfooter/copyrightfooter.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex flex-row copyright-bg align-items-center text-white\">\n    <div class=\"copyright align-self-center  ml-auto\" style=\"padding-left: 10px;\">\n    © 2019 Department of Sainik Welfare, Maharashtra State. \n    </div>\n</div>"

/***/ }),

/***/ "./src/app/copyrightfooter/copyrightfooter.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/copyrightfooter/copyrightfooter.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".copyright-bg {\n  background-color: #162f5d; }\n\n.copyright {\n  font-size: 0.8rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2hydXNoaWtlc2gvd29yay9kc3cvc3JjL2FwcC9jb3B5cmlnaHRmb290ZXIvY29weXJpZ2h0Zm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUkseUJBQXlCLEVBQUE7O0FBRTdCO0VBRUksNEJBQTJCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb3B5cmlnaHRmb290ZXIvY29weXJpZ2h0Zm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvcHlyaWdodC1iZ1xue1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxNjJmNWQ7XG59XG4uY29weXJpZ2h0XG57XG4gICAgZm9udC1zaXplOiAwLjhyZW0haW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/copyrightfooter/copyrightfooter.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/copyrightfooter/copyrightfooter.component.ts ***!
  \**************************************************************/
/*! exports provided: CopyrightfooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CopyrightfooterComponent", function() { return CopyrightfooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CopyrightfooterComponent = /** @class */ (function () {
    function CopyrightfooterComponent() {
    }
    CopyrightfooterComponent.prototype.ngOnInit = function () {
    };
    CopyrightfooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-copyrightfooter',
            template: __webpack_require__(/*! ./copyrightfooter.component.html */ "./src/app/copyrightfooter/copyrightfooter.component.html"),
            styles: [__webpack_require__(/*! ./copyrightfooter.component.scss */ "./src/app/copyrightfooter/copyrightfooter.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CopyrightfooterComponent);
    return CopyrightfooterComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex flex-row app-footer p-2 text-white\">\n  <div class=\"align-self-center\">\n    <ul class=\"footerlist p-2\">\n      <li>\n        Publicity\n      </li>\n      <li>\n        About Us\n      </li>\n      <li>\n        Home\n      </li>\n      <li>\n        Site Map\n      </li>\n      <li>\n        Download App\n      </li>\n    </ul>\n  </div>\n  <div class=\"align-self-center\" style=\"padding-left: 10px;\">\n\n    <div class=\"d-flex flex-column \">\n      <div >Photo Gallery</div>\n      <div ><img src=\"assets/1.jpg\" alt=\"...\" class=\"galleryimage img-thumbnail\"></div>\n    </div>\n\n\n\n  </div>\n  <div class=\"align-self-center\" style=\"padding-left: 10px;\">\n\n      <div class=\"d-flex flex-column \">\n        <div >Video Gallery</div>\n        <div ><img src=\"assets/video.jpg\" alt=\"...\" class=\"galleryimage img-thumbnail\"></div>\n      </div>\n  \n  \n  \n  </div>\n  <div class=\"copyright ml-auto align-self-center\" style=\"padding-left: 10px;\">\n\n      © This is Official <br>Website of Department of <br>Sainik Welfare, <br>Maharashtra State.<br>All Rights Reserved. \n  \n  \n  </div>\n  <div class=\" address ml-auto align-self-center\" style=\"padding-left: 10px;\"  >\n\n      <strong>Address:</strong><br>\n      \"Raigad Building\",<br>\n      Opposite National War Memorial,<br>\n      Ghorpadi, Pune-411001<br>\n\n  \n  \n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/footer/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app-footer {\n  background-color: #5e6d8e !important; }\n\n.footerlist {\n  list-style: none;\n  margin: 0;\n  padding: 0;\n  font-size: 0.8rem !important; }\n\n.galleryimage {\n  height: 120px; }\n\n.address {\n  font-size: 0.8rem !important; }\n\n.copyright {\n  font-size: 0.8rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2hydXNoaWtlc2gvd29yay9kc3cvc3JjL2FwcC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksb0NBQW9DLEVBQUE7O0FBRXhDO0VBQ0ksZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxVQUFVO0VBQ1YsNEJBQTJCLEVBQUE7O0FBRS9CO0VBRUksYUFBYSxFQUFBOztBQUVqQjtFQUVJLDRCQUEyQixFQUFBOztBQUcvQjtFQUVJLDRCQUEyQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hcHAtZm9vdGVyXG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzVlNmQ4ZSAhaW1wb3J0YW50O1xufVxuLmZvb3Rlcmxpc3R7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbiAgICBmb250LXNpemU6IDAuOHJlbSFpbXBvcnRhbnQ7XG59XG4uZ2FsbGVyeWltYWdlXG57XG4gICAgaGVpZ2h0OiAxMjBweDtcbn1cbi5hZGRyZXNzXG57XG4gICAgZm9udC1zaXplOiAwLjhyZW0haW1wb3J0YW50O1xufVxuXG4uY29weXJpZ2h0XG57XG4gICAgZm9udC1zaXplOiAwLjhyZW0haW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/footer/footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"d-flex flex-row app-header\">\n    <div >\n        <img height=\"100\" src=\"assets/emblem-dark.png\" alt=\"national emblem\"/>\n    </div>\n    <div class=\"align-self-center\" style=\"padding-left: 10px;\">\n        <div class=\"row no-gutters\">\n            <div class=\"col header-logo-item\">\n                <strong lang=\"hi\">माजी सैनिक कल्याण विभाग</strong>\n            </div>\n        </div>\n        <div class=\"row no-gutters\">\n            <div class=\"col header-logo-item\">\n                DEPARTMENT OF\n            </div>\n        </div>\n        <div class=\"row no-gutters\">\n            <div class=\"col header-logo-item\">\n                Sainik Welfare\n            </div>\n        </div>\n    </div>\n    \n    <div class=\"ml-auto\">\n        <img src=\"assets/swach-bharat.png\" alt=\"Swach Bharat\"/>\n    </div>\n  </div>\n\n<!-- <div>\n    <div class=\"row no-gutters\">\n      <div class=\"col-1\">\n          <img height=\"100\" src=\"assets/emblem-dark.png\" alt=\"national emblem\"/>\n      </div>\n      <div class=\"col-3\">\n          <div class=\"row no-gutters\">\n              <div class=\"col header-logo-item\">\n                  <strong lang=\"hi\">माजी सैनिक कल्याण विभाग</strong>\n              </div>\n          </div>\n          <div class=\"row no-gutters\">\n              <div class=\"col header-logo-item\">\n                  DEPARTMENT OF\n              </div>\n          </div>\n          <div class=\"row no-gutters\">\n              <div class=\"col header-logo-item\">\n                  Sainik Welfare\n              </div>\n          </div>\n      </div>\n      <div class=\"col-8\"></div>\n\n    </div>\n  </div> -->"

/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app-header {\n  padding: 10px; }\n\n.logo strong {\n  font-weight: 600;\n  display: block;\n  font-size: 80%; }\n\n.logo {\n  font-size: 160%; }\n\n.logo span {\n  display: block;\n  font-weight: 900;\n  font-size: 110%; }\n\n.logo mat-list-item {\n  height: unset; }\n\n.no-gutters {\n  margin-right: 0;\n  margin-left: 0; }\n\n.no-gutters > .col,\n  .no-gutters > [class*=\"col-\"] {\n    padding-right: 0;\n    padding-left: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2hydXNoaWtlc2gvd29yay9kc3cvc3JjL2FwcC9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksYUFBYSxFQUFBOztBQUVqQjtFQUNJLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsY0FBYyxFQUFBOztBQUVsQjtFQUVJLGVBQWUsRUFBQTs7QUFFbkI7RUFDSSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHbkI7RUFFSSxhQUFhLEVBQUE7O0FBSWpCO0VBQ0ksZUFBZTtFQUNmLGNBQWMsRUFBQTs7QUFGbEI7O0lBTU0sZ0JBQWdCO0lBQ2hCLGVBQWUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYXBwLWhlYWRlclxue1xuICAgIHBhZGRpbmc6IDEwcHg7XG59XG4ubG9nbyBzdHJvbmcge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgZm9udC1zaXplOiA4MCU7XG59XG4ubG9nb1xue1xuICAgIGZvbnQtc2l6ZTogMTYwJTtcbn1cbi5sb2dvIHNwYW4ge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4gICAgZm9udC1zaXplOiAxMTAlO1xufVxuXG4ubG9nbyBtYXQtbGlzdC1pdGVtXG57XG4gICAgaGVpZ2h0OiB1bnNldDtcbn1cblxuXG4ubm8tZ3V0dGVycyB7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICAgIG1hcmdpbi1sZWZ0OiAwO1xuICBcbiAgICA+IC5jb2wsXG4gICAgPiBbY2xhc3MqPVwiY29sLVwiXSB7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xuICAgICAgcGFkZGluZy1sZWZ0OiAwO1xuICAgIH1cbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/homepage/carousel/carousel.component.html":
/*!***********************************************************!*\
  !*** ./src/app/homepage/carousel/carousel.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ngb-carousel *ngIf=\"images\" >\n    <ng-template ngbSlide *ngFor=\"let image of images\">\n      <img [src]=\"image\" class=\"d-block w-100\"  alt=\"Random slide\">\n      <div class=\"carousel-caption\">\n        <h3>No mouse navigation</h3>\n        <p>This carousel hides navigation arrows and indicators.</p>\n      </div>\n    </ng-template>\n  </ngb-carousel>\n  "

/***/ }),

/***/ "./src/app/homepage/carousel/carousel.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/homepage/carousel/carousel.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWVwYWdlL2Nhcm91c2VsL2Nhcm91c2VsLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/homepage/carousel/carousel.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/homepage/carousel/carousel.component.ts ***!
  \*********************************************************/
/*! exports provided: CarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselComponent", function() { return CarouselComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");



var CarouselComponent = /** @class */ (function () {
    function CarouselComponent(config) {
        this.showNavigationArrows = false;
        this.showNavigationIndicators = false;
        this.images = ['assets/1.jpg', 'assets/2.jpg'];
        // customize default values of carousels used by this component tree
        config.showNavigationArrows = true;
        config.showNavigationIndicators = true;
    }
    CarouselComponent.prototype.ngOnInit = function () {
    };
    CarouselComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-carousel',
            template: __webpack_require__(/*! ./carousel.component.html */ "./src/app/homepage/carousel/carousel.component.html"),
            styles: [__webpack_require__(/*! ./carousel.component.scss */ "./src/app/homepage/carousel/carousel.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCarouselConfig"]])
    ], CarouselComponent);
    return CarouselComponent;
}());



/***/ }),

/***/ "./src/app/homepage/home/home.component.html":
/*!***************************************************!*\
  !*** ./src/app/homepage/home/home.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid py-2\" >\n\n        <div class=\"row\">\n          <div class=\"col-9\">\n              <app-carousel></app-carousel>\n          </div>\n          <div class=\"col-3\">\n            <app-quickaccess></app-quickaccess>\n          </div>\n\n        </div>\n        <div class=\"row\">\n            <div class=\"col-6\">\n          <app-presslinks></app-presslinks>\n            </div>\n            <div class=\"col-3\">\n                <app-quicklinks></app-quicklinks>\n            </div>\n            <div class=\"col-3\">\n                <app-sociallinks></app-sociallinks>\n              </div>\n        </div>\n\n\n\n</div>"

/***/ }),

/***/ "./src/app/homepage/home/home.component.scss":
/*!***************************************************!*\
  !*** ./src/app/homepage/home/home.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWVwYWdlL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/homepage/home/home.component.ts":
/*!*************************************************!*\
  !*** ./src/app/homepage/home/home.component.ts ***!
  \*************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/homepage/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/homepage/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/homepage/homepage.module.ts":
/*!*********************************************!*\
  !*** ./src/app/homepage/homepage.module.ts ***!
  \*********************************************/
/*! exports provided: HomepageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageModule", function() { return HomepageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./carousel/carousel.component */ "./src/app/homepage/carousel/carousel.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/homepage/home/home.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _quickaccess_quickaccess_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./quickaccess/quickaccess.component */ "./src/app/homepage/quickaccess/quickaccess.component.ts");
/* harmony import */ var _presslinks_presslinks_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./presslinks/presslinks.component */ "./src/app/homepage/presslinks/presslinks.component.ts");
/* harmony import */ var _quicklinks_quicklinks_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./quicklinks/quicklinks.component */ "./src/app/homepage/quicklinks/quicklinks.component.ts");
/* harmony import */ var _sociallinks_sociallinks_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./sociallinks/sociallinks.component */ "./src/app/homepage/sociallinks/sociallinks.component.ts");










var HomepageModule = /** @class */ (function () {
    function HomepageModule() {
    }
    HomepageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_3__["CarouselComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"], _quickaccess_quickaccess_component__WEBPACK_IMPORTED_MODULE_6__["QuickaccessComponent"], _presslinks_presslinks_component__WEBPACK_IMPORTED_MODULE_7__["PresslinksComponent"], _quicklinks_quicklinks_component__WEBPACK_IMPORTED_MODULE_8__["QuicklinksComponent"], _sociallinks_sociallinks_component__WEBPACK_IMPORTED_MODULE_9__["SociallinksComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"]
            ],
            exports: [_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"], _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_3__["CarouselComponent"]]
        })
    ], HomepageModule);
    return HomepageModule;
}());



/***/ }),

/***/ "./src/app/homepage/presslinks.service.ts":
/*!************************************************!*\
  !*** ./src/app/homepage/presslinks.service.ts ***!
  \************************************************/
/*! exports provided: PresslinksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PresslinksService", function() { return PresslinksService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var PresslinksService = /** @class */ (function () {
    function PresslinksService(http) {
        this.http = http;
        this.pressitems = [
            {
                url: "www.google.com",
                description: "Google"
            },
            {
                url: "www.google.com",
                description: "Google2"
            },
            {
                url: "www.google.com",
                description: "Google3"
            },
            {
                url: "www.google.com",
                description: "Google4"
            }
        ];
    }
    PresslinksService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    PresslinksService.prototype.endpoint = function (collectionName) {
        return "/cockpit/api/collections/get/" + collectionName + "?token=48c21bd90fe7b4faef2c20cf33d761";
    };
    PresslinksService.prototype.getPressLinks = function () {
        return this.http.get(this.endpoint('presslinks')).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.extractData));
    };
    PresslinksService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], PresslinksService);
    return PresslinksService;
}());



/***/ }),

/***/ "./src/app/homepage/presslinks/presslinks.component.html":
/*!***************************************************************!*\
  !*** ./src/app/homepage/presslinks/presslinks.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card mt-2\">\n  <div class=\"card-header bg-info text-white\">\n        Press Note\n  </div>\n  <div class=\"card-body\">\n    <ul>\n      <li *ngFor=\"let pressItem of pressitems\">\n        <a [href]=\"pressItem.englishurl\">{{pressItem.englishdescription}}</a>\n      </li>\n     </ul>\n  </div>\n  <div class=\"card-footer \">\n      <a href=\"pressItem.englishurl\">Read More</a>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/homepage/presslinks/presslinks.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/homepage/presslinks/presslinks.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWVwYWdlL3ByZXNzbGlua3MvcHJlc3NsaW5rcy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/homepage/presslinks/presslinks.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/homepage/presslinks/presslinks.component.ts ***!
  \*************************************************************/
/*! exports provided: PresslinksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PresslinksComponent", function() { return PresslinksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _presslinks_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../presslinks.service */ "./src/app/homepage/presslinks.service.ts");



var PresslinksComponent = /** @class */ (function () {
    function PresslinksComponent(pressLinksService) {
        this.pressLinksService = pressLinksService;
        this.pressitems = [];
    }
    PresslinksComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pressLinksService.getPressLinks().subscribe(function (data) {
            _this.pressitems = data.entries;
        });
    };
    PresslinksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-presslinks',
            template: __webpack_require__(/*! ./presslinks.component.html */ "./src/app/homepage/presslinks/presslinks.component.html"),
            styles: [__webpack_require__(/*! ./presslinks.component.scss */ "./src/app/homepage/presslinks/presslinks.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_presslinks_service__WEBPACK_IMPORTED_MODULE_2__["PresslinksService"]])
    ], PresslinksComponent);
    return PresslinksComponent;
}());



/***/ }),

/***/ "./src/app/homepage/quickaccess/quickaccess.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/homepage/quickaccess/quickaccess.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid py-2\">\n\n\n  <div class=\"row\" class=\"bg-info p-3 text-white\">\n    <div class=\"d-flex flex-row app-header\">\n      <div class=\"align-self-center\">\n        <i class=\"material-icons\" style=\"font-size:40px;\">people_outline</i>\n      </div>\n      <div class=\"align-self-center\" style=\"padding-left: 10px;\">\n        Citizen Charter Infortmation Of Citizen Charter\n      </div>\n\n    </div>\n\n\n  </div>\n\n\n\n  <div class=\"row\" class=\"bg-info my-2 p-3 text-white\">\n    <div class=\"d-flex flex-row app-header\">\n      <div class=\"align-self-center\">\n        <img height=\"40\" src=\"assets/logo.png\" alt=\"national emblem\" />\n      </div>\n      <div class=\"align-self-center\" style=\"padding-left: 10px;\">\n        Flag Day Fund Armed Forces Flag Day\n      </div>\n\n    </div>\n\n\n  </div>\n\n  <div class=\"row\" class=\"bg-info my-2 p-3 text-white\">\n    <div class=\"d-flex flex-row app-header\">\n      <div class=\"align-self-center\">\n        <i class=\"material-icons\" style=\"font-size:40px;\">monetization_on</i>\n      </div>\n      <div class=\"align-self-center\" style=\"padding-left: 10px;\">\n        Pension Project Ex-Servicemen Pension\n      </div>\n\n    </div>\n\n\n  </div>\n\n\n  <div class=\"row\" class=\"p-3\">\n      <img height=\"200\" src=\"assets/frame.png\" alt=\"national emblem\" />\n    </div>\n  \n\n\n\n\n</div>"

/***/ }),

/***/ "./src/app/homepage/quickaccess/quickaccess.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/homepage/quickaccess/quickaccess.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWVwYWdlL3F1aWNrYWNjZXNzL3F1aWNrYWNjZXNzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/homepage/quickaccess/quickaccess.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/homepage/quickaccess/quickaccess.component.ts ***!
  \***************************************************************/
/*! exports provided: QuickaccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuickaccessComponent", function() { return QuickaccessComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var QuickaccessComponent = /** @class */ (function () {
    function QuickaccessComponent() {
    }
    QuickaccessComponent.prototype.ngOnInit = function () {
    };
    QuickaccessComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-quickaccess',
            template: __webpack_require__(/*! ./quickaccess.component.html */ "./src/app/homepage/quickaccess/quickaccess.component.html"),
            styles: [__webpack_require__(/*! ./quickaccess.component.scss */ "./src/app/homepage/quickaccess/quickaccess.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], QuickaccessComponent);
    return QuickaccessComponent;
}());



/***/ }),

/***/ "./src/app/homepage/quicklinks.service.ts":
/*!************************************************!*\
  !*** ./src/app/homepage/quicklinks.service.ts ***!
  \************************************************/
/*! exports provided: QuicklinksService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuicklinksService", function() { return QuicklinksService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var QuicklinksService = /** @class */ (function () {
    function QuicklinksService(http) {
        this.http = http;
    }
    QuicklinksService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    QuicklinksService.prototype.endpoint = function (collectionName) {
        return "/cockpit/api/collections/get/" + collectionName + "?token=48c21bd90fe7b4faef2c20cf33d761";
    };
    QuicklinksService.prototype.getQuickLinks = function () {
        return this.http.get(this.endpoint('quicklinks')).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.extractData));
    };
    QuicklinksService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], QuicklinksService);
    return QuicklinksService;
}());



/***/ }),

/***/ "./src/app/homepage/quicklinks/quicklinks.component.html":
/*!***************************************************************!*\
  !*** ./src/app/homepage/quicklinks/quicklinks.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card mt-2\">\n    <div class=\"card-header bg-info text-white\">\n          Quick Links\n    </div>\n    <div class=\"card-body\">\n      <ul>\n        <li *ngFor=\"let quickLinkItem of quickLinkItems\">\n          <a [href]=\"quickLinkItem.englishurl\">{{quickLinkItem.englishdescription}}</a>\n        </li>\n       </ul>\n    </div>\n  \n  </div>"

/***/ }),

/***/ "./src/app/homepage/quicklinks/quicklinks.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/homepage/quicklinks/quicklinks.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWVwYWdlL3F1aWNrbGlua3MvcXVpY2tsaW5rcy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/homepage/quicklinks/quicklinks.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/homepage/quicklinks/quicklinks.component.ts ***!
  \*************************************************************/
/*! exports provided: QuicklinksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuicklinksComponent", function() { return QuicklinksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _quicklinks_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../quicklinks.service */ "./src/app/homepage/quicklinks.service.ts");



var QuicklinksComponent = /** @class */ (function () {
    function QuicklinksComponent(quickLinksService) {
        this.quickLinksService = quickLinksService;
        this.quickLinkItems = [];
    }
    QuicklinksComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.quickLinksService.getQuickLinks().subscribe(function (data) {
            _this.quickLinkItems = data.entries;
        });
    };
    QuicklinksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-quicklinks',
            template: __webpack_require__(/*! ./quicklinks.component.html */ "./src/app/homepage/quicklinks/quicklinks.component.html"),
            styles: [__webpack_require__(/*! ./quicklinks.component.scss */ "./src/app/homepage/quicklinks/quicklinks.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_quicklinks_service__WEBPACK_IMPORTED_MODULE_2__["QuicklinksService"]])
    ], QuicklinksComponent);
    return QuicklinksComponent;
}());



/***/ }),

/***/ "./src/app/homepage/sociallinks/sociallinks.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/homepage/sociallinks/sociallinks.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card mt-2\">\n  <div class=\"card-header bg-info text-white\">\n    Social Links\n  </div>\n  <div class=\"card-body\">\n    <ul class=\"sociallist\">\n      <li>\n        <a>\n          <i class=\"fab fa-facebook facebook\"></i> Facebook\n        </a>\n      </li>\n      <li>\n        <a>\n\n          <i class=\"fab fa-youtube-square youtube\"></i> YouTube\n        </a>\n      </li>\n      <li>\n        <a>\n          <i class=\"fab fa-blogger blogger\"></i> E Blog\n        </a>\n      </li>\n      <li>\n        <a>\n          <i class=\"fab fa-google-plus-square gplus\"></i> Google Plus\n        </a>\n      </li>\n      <li>\n        <a>\n          <i class=\"fab fa-google-play playstore\"></i> Google Play\n        </a>\n      </li>\n    </ul>\n\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/homepage/sociallinks/sociallinks.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/homepage/sociallinks/sociallinks.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sociallist {\n  list-style: none;\n  margin: 0;\n  padding: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2hydXNoaWtlc2gvd29yay9kc3cvc3JjL2FwcC9ob21lcGFnZS9zb2NpYWxsaW5rcy9zb2NpYWxsaW5rcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFnQjtFQUNoQixTQUFTO0VBQ1QsVUFBVSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvaG9tZXBhZ2Uvc29jaWFsbGlua3Mvc29jaWFsbGlua3MuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc29jaWFsbGlzdHtcbiAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/homepage/sociallinks/sociallinks.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/homepage/sociallinks/sociallinks.component.ts ***!
  \***************************************************************/
/*! exports provided: SociallinksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SociallinksComponent", function() { return SociallinksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SociallinksComponent = /** @class */ (function () {
    function SociallinksComponent() {
    }
    SociallinksComponent.prototype.ngOnInit = function () {
    };
    SociallinksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sociallinks',
            template: __webpack_require__(/*! ./sociallinks.component.html */ "./src/app/homepage/sociallinks/sociallinks.component.html"),
            styles: [__webpack_require__(/*! ./sociallinks.component.scss */ "./src/app/homepage/sociallinks/sociallinks.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SociallinksComponent);
    return SociallinksComponent;
}());



/***/ }),

/***/ "./src/app/language.service.ts":
/*!*************************************!*\
  !*** ./src/app/language.service.ts ***!
  \*************************************/
/*! exports provided: LanguageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageService", function() { return LanguageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LanguageService = /** @class */ (function () {
    function LanguageService() {
        this.currentLanguage = 'english';
    }
    LanguageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LanguageService);
    return LanguageService;
}());



/***/ }),

/***/ "./src/app/menus/menus.service.ts":
/*!****************************************!*\
  !*** ./src/app/menus/menus.service.ts ***!
  \****************************************/
/*! exports provided: MenusService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenusService", function() { return MenusService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var MenusService = /** @class */ (function () {
    function MenusService(http) {
        this.http = http;
    }
    MenusService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    MenusService.prototype.endpoint = function (collectionName) {
        return "/cockpit/api/collections/get/" + collectionName + "?token=48c21bd90fe7b4faef2c20cf33d761";
    };
    MenusService.prototype.getMenus = function () {
        return this.http.get(this.endpoint('menus')).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.extractData));
    };
    MenusService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], MenusService);
    return MenusService;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light dsw-navbar\">\n    <a class=\"navbar-brand\" href=\"#\">\n        <img src=\"assets/logo.png\" height=\"40\"/>\n    </a>\n  \n    <button class=\"navbar-toggler\" type=\"button\" (click)=\"toggleNavbar()\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n  \n  \n    <div class=\"collapse navbar-collapse\" [ngClass]=\"{ 'show': navbarOpen }\">\n      <ul class=\"navbar-nav mr-auto\">\n        <li class=\"nav-item\" *ngFor=\"let menu of menus\">\n          <a class=\"nav-link\" [routerLink]=\"menu.url\">{{menu.english}}</a>\n        </li>\n  \n        \n      </ul>\n    </div>\n  </nav>\n  "

/***/ }),

/***/ "./src/app/navbar/navbar.component.scss":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dsw-navbar {\n  background-color: #5e6d8e !important;\n  padding-top: 0;\n  padding-bottom: 0; }\n\n.dsw-navbar .nav-link:hover {\n  background-color: #04b954 !important; }\n\n.dsw-navbar .nav-link {\n  color: white !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2hydXNoaWtlc2gvd29yay9kc3cvc3JjL2FwcC9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0NBQW1DO0VBQ25DLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTs7QUFFckI7RUFFSSxvQ0FBb0MsRUFBQTs7QUFFeEM7RUFJSSx1QkFBcUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZHN3LW5hdmJhcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWU2ZDhlIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMDtcbn1cbi5kc3ctbmF2YmFyIC5uYXYtbGluazpob3Zlclxue1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwNGI5NTQgIWltcG9ydGFudDtcbn1cbi5kc3ctbmF2YmFyIC5uYXYtbGlua1xue1xuICAgIFxuICAgIFxuICAgIGNvbG9yOndoaXRlIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _menus_menus_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../menus/menus.service */ "./src/app/menus/menus.service.ts");



var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(menuService) {
        this.menuService = menuService;
        this.navbarOpen = false;
        this.menus = [];
    }
    NavbarComponent.prototype.getMenus = function () {
        var _this = this;
        this.menuService.getMenus().subscribe(function (data) {
            console.log(data.entries);
            _this.menus = data.entries;
        });
    };
    NavbarComponent.prototype.ngOnInit = function () {
        this.getMenus();
    };
    NavbarComponent.prototype.toggleNavbar = function () {
        this.navbarOpen = !this.navbarOpen;
    };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/navbar/navbar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_menus_menus_service__WEBPACK_IMPORTED_MODULE_2__["MenusService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.module.ts":
/*!*****************************************!*\
  !*** ./src/app/navbar/navbar.module.ts ***!
  \*****************************************/
/*! exports provided: NavbarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarModule", function() { return NavbarModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"], _header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"]],
            exports: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"], _header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"]
            ]
        })
    ], NavbarModule);
    return NavbarModule;
}());



/***/ }),

/***/ "./src/app/sanitize-html.pipe.ts":
/*!***************************************!*\
  !*** ./src/app/sanitize-html.pipe.ts ***!
  \***************************************/
/*! exports provided: SanitizeHtmlPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SanitizeHtmlPipe", function() { return SanitizeHtmlPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");



var SanitizeHtmlPipe = /** @class */ (function () {
    function SanitizeHtmlPipe(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    SanitizeHtmlPipe.prototype.transform = function (v) {
        return this._sanitizer.sanitize(_angular_core__WEBPACK_IMPORTED_MODULE_1__["SecurityContext"].HTML || _angular_core__WEBPACK_IMPORTED_MODULE_1__["SecurityContext"].STYLE, v);
    };
    SanitizeHtmlPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'sanitizeHtml'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], SanitizeHtmlPipe);
    return SanitizeHtmlPipe;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/hrushikesh/work/dsw/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map